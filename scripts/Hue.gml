var cmin, cmax, r, g, b, val;
    r = Red(argument0);
    g = Green(argument0);
    b = Blue(argument0);
    cmax = max(r, g, b);
    cmin = min(r, g, b);
    if (cmax - cmin == 0) { return 0; }
    r = (r - cmin) / (cmax - cmin);
    g = (g - cmin) / (cmax - cmin);
    b = (b - cmin) / (cmax - cmin);
    cmax = max(r, g, b);
    if (cmax == r) {
        val = 42.5 * (g - b);
        if (val < 0) { val += 255; }
    } else if (cmax == g) {
        val = 85 + 42.5 * (b - r);
    } else {
        val = 170 + 42.5 * (r - g);
    }
    return floor(abs(val mod 255));
