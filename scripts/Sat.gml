var cmin, cmax;
    cmin = min(Red(argument0), Green(argument0), Blue(argument0));
    cmax = max(Red(argument0), Green(argument0), Blue(argument0));
    return cmax - cmin;
