
        STATE = "menu";
        PAUSED = false;
        menuTargetWidth   = 190;
        menuTargetHeight  = 160;
        menuTargetX       = (room_width-menuTargetWidth)/2;
        menuTargetY       = (room_height-menuTargetHeight)/2;
        hueSpeed = 0.1;
        hueMax = 4;
        var cell;
        for (i = 0; i < gridWidth; i++) {
            for (j = 0; j < gridHeight; j++) {
                cell = ds_grid_get(grid, i, j);
                cell.targetCol = make_color_hsv(random(255), 200, 255);
            }
        }
