/*
 * LoadLevel(levelNum)
 *
 */
var cell;
switch (argument0) {
    case 1:
        colGrid = Level_1_Col();
        dirGrid = Level_1_Dir();
        break;
    case 2:
        colGrid = Level_2_Col();
        dirGrid = Level_2_Dir();
        break;
    case 3:
        colGrid = Level_3_Col();
        dirGrid = Level_3_Dir();
        break;
    case 4:
        colGrid = Level_4_Col();
        dirGrid = Level_4_Dir();
        break;
    case 5:
        colGrid = Level_5_Col();
        dirGrid = Level_5_Dir();
        break;
    case 6:
        colGrid = Level_6_Col();
        dirGrid = Level_6_Dir();
        break;
    case 7:
        colGrid = Level_7_Col();
        dirGrid = Level_7_Dir();
        break;
    case 8:
        colGrid = Level_8_Col();
        dirGrid = Level_8_Dir();
        break;
}

for (i = 0; i < ROOT.gridWidth; i++) {
    for (j = 0; j < ROOT.gridHeight; j++) {
        cell = ds_grid_get(ROOT.grid, i, j);
        cell.dominant = false;
        cell.targetCol = ds_grid_get(colGrid, i, j);
        switch (ds_grid_get(dirGrid, i, j)) {
            case 0: cell.bleedX =  0; cell.bleedY =  0; break;
            case 1: cell.bleedX =  0; cell.bleedY = -1; break;
            case 2: cell.bleedX =  1; cell.bleedY = -1; break;
            case 3: cell.bleedX =  1; cell.bleedY =  0; break;
            case 4: cell.bleedX =  1; cell.bleedY =  1; break;
            case 5: cell.bleedX =  0; cell.bleedY =  1; break;
            case 6: cell.bleedX = -1; cell.bleedY =  1; break;
            case 7: cell.bleedX = -1; cell.bleedY =  0; break;
            case 8: cell.bleedX = -1; cell.bleedY =  1; break;
        }
    }
}
ds_grid_destroy(colGrid);
ds_grid_destroy(dirGrid);
