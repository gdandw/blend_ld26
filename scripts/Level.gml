/*
 * Level(level, val)
 *
 */
 
switch (argument1) {
    case "hue":
        switch (argument0) {
            case 1:
                return hue < 120;
                break;
            case 2:
                return hue > 25;
                break;
            case 3:
                return hue > 50;
                break;
            case 4:
                return hue < 160;
                break;
            case 5:
                return hue < 14;
                break;
            case 6:
                return hue < 100;
                break;
            case 7:
                return hue < 25;
                break;
            case 8:
                return hue < 120;
                break;
            default:
                return hue > 255;
                break;
        }
        break;
        
    case "color": // brush color
        switch (argument0) {
            case 1:
            case 7:
            case 8:
                return c_red; break;
            case 2:
                return c_aqua; break;
            case 3:
                return c_aqua; break;
            case 4:
                return c_lime; break;
            case 5:
                return c_red; break;
            case 6:
                return c_yellow; break;
            default:
                return c_lime; break;
        }
        break;
        
    case "bleed": // bleed amount
        switch (argument0) {
            default:
                return 0.2; break;
        }
        break;
        
    case "brush": // brush time
        switch (argument0) {
            case 3:
            case 5:
            case 6:
            case 7:
                return room_speed * 2; break;
            case 8:
                return room_speed; break;
            default:
                return room_speed * 0.4; break;
        }
        break;

    case "blend": // blend time
        switch (argument0) {
            case 1:
            case 3:
            case 4:
            case 6:
            case 7:
                return room_speed * 5;
            default:
                return room_speed * 3; break;
        }
        break;
}
