/*
 * TweenColor(fromCol, toCol, amount)
 *
 */
var fromCol, toCol, amount;
    fromCol = argument0;
    toCol = argument1;
    amount = argument2;
    
    return make_color_rgb(
        Tween(color_get_red(fromCol), color_get_red(toCol), amount),
        Tween(color_get_green(fromCol), color_get_green(toCol), amount),
        Tween(color_get_blue(fromCol), color_get_blue(toCol), amount)
    );
